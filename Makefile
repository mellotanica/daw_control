builddir = build
bin = $(builddir)/daw_control.ino.hex
sources := daw_control/daw_control.ino $(wildcard daw_control/*)
pysources := setup.py requirements.txt $(wildcard dawcontrolconfig/*.py)

BOARD ?= arduino:avr:leonardo
COMPORT ?= /dev/ttyACM0

TARGET_BOARD ?= dawcontrol
TARGET_NAME ?= Daw Controller
TARGET_VENDOR_ID ?= 0xB00B
TARGET_PRODUCT_ID ?= 0x0A55

ARDUINO_DATA = $(shell arduino-cli config dump | grep 'data: ' | sed -e 's/.*: //')
BOARD_QUERY = $(subst /,.*,$(dir $(subst :,/,${BOARD})))
BOARD_NAME = $(notdir $(subst :,/,${BOARD}))
BOARDS_FILE = $(shell find "${ARDUINO_DATA}" -name "boards.txt" | grep "${BOARDS_QUERY}")
BUILD_BOARD = $(subst /,:,$(dir $(subst :,/,${BOARD})))${TARGET_BOARD}

all: $(bin)

$(bin): $(sources) ${BOARDS_FILE}
	@sed -e '/${TARGET_BOARD}\..*/d' -e 's/\(${BOARD_NAME}\)\(\..*\)/\1\2\n${TARGET_BOARD}\2/g' -e 's/\(${TARGET_BOARD}\.build\.usb_product="\).*/\1${TARGET_NAME}"/' -e 's/\(${TARGET_BOARD}\.build\.pid=\).*/\1${TARGET_PRODUCT_ID}/' -e 's/\(${TARGET_BOARD}\.build\.vid=\).*/\1${TARGET_VENDOR_ID}/' -i ${BOARDS_FILE}
	@mkdir -p build .cache
	@arduino-cli compile \
		-b ${BUILD_BOARD} \
		--output-dir $(builddir) \
		--build-cache-path .cache \
		$<

.PHONY: flash
flash: $(bin)
	@arduino-cli upload \
		-b ${BUILD_BOARD} \
		-i $< \
		-p ${COMPORT}

.PHONY: install
install: $(pysources)
	@pip install -r requirements.txt

.PHONY: appimage
appimage: $(pysources) dawcontrol.png build_appimage.sh
	@./build_appimage.sh

clean:
	@rm -rf build/* build_appimage/* *.egg-info 

distclean: clean
	@rm -rf .cache/* build build_appimage DawControl-*.AppImage
