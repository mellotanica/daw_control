# Daw Controller

This project implements firmware and configuration tools to build a control pedalboard suitable for all music-relate stuff. The pedalboard will be able to send midi note events as well as keyboard inputs, this way no daw/program will be left behind.

The base project is built upon an Arduino Leonardo replica chip, with 5 configurable footswitches.

The documentation can be found [here](https://gitlab.com/mellotanica/daw_control/-/wikis/home)

To configure the board use the [online configuration tool](https://mellotanica.gitlab.io/daw_control/)
