#!/usr/bin/env bash

BUILD_DIR=build_appimage 
THIS_COMMIT=$(git rev-list HEAD | head -n 1)
VERSION=$(git describe --tags)
TAG_COMMIT=$(git rev-list ${VERSION} | head -n 1)

if "$THIS_COMMIT" != "$TAG_COMMIT"; then
    VERSION="${VERSION}-$(echo "$THIS_COMMIT" | cut -c '1-8')"
fi
export VERSION

mkdir -p $BUILD_DIR
pushd $BUILD_DIR

    # Download an AppImage of Python 3.8 built for manylinux
    wget https://github.com/niess/python-appimage/releases/download/python3.8/python3.8.13-cp38-cp38-manylinux1_x86_64.AppImage

    # Extract this AppImage
    chmod +x python*-manylinux1_x86_64.AppImage
    ./python*-manylinux1_x86_64.AppImage --appimage-extract

popd
# Install dawcontrol into the extracted AppDir
./$BUILD_DIR/squashfs-root/AppRun -m pip install -r ./requirements.txt

pushd $BUILD_DIR
    # Change AppRun so that it launches dawcontrol
    sed -i -e 's|/opt/python3.8/bin/python3.8|/usr/bin/dawcontrol|g' ./squashfs-root/AppRun

    # Test that it works
    #./squashfs-root/AppRun

    # Edit the desktop file
    mv squashfs-root/usr/share/applications/python3.8*.desktop squashfs-root/usr/share/applications/dawcontrol.desktop
    sed -i -e 's|^Name=.*|Name=DawControl|g' squashfs-root/usr/share/applications/*.desktop
    sed -i -e 's|^Exec=.*|Exec=dawcontrol|g' squashfs-root/usr/share/applications/*.desktop
    sed -i -e 's|^Icon=.*|Icon=dawcontrol|g' squashfs-root/usr/share/applications/*.desktop
    sed -i -e 's|^Comment=.*|Comment=Free and open source qualitative research tool|g' squashfs-root/usr/share/applications/*.desktop
    sed -i -e 's|^Terminal=.*|Terminal=false|g' squashfs-root/usr/share/applications/*.desktop
    sed -i -e 's|^Categories=.*|Categories=Science;|g' squashfs-root/usr/share/applications/*.desktop
    rm squashfs-root/*.desktop
    cp squashfs-root/usr/share/applications/*.desktop squashfs-root/

    # Add icon
    mkdir -p squashfs-root/usr/share/icons/hicolor/256x256/apps/
    cp ../dawcontrol.png squashfs-root/usr/share/icons/hicolor/256x256/apps/dawcontrol.png
    cp squashfs-root/usr/share/icons/hicolor/256x256/apps/dawcontrol.png squashfs-root/

    # Convert back into an AppImage
    wget -c https://github.com/$(wget -q https://github.com/probonopd/go-appimage/releases -O - | grep "appimagetool-.*-x86_64.AppImage" | head -n 1 | cut -d '"' -f 2)
    chmod +x appimagetool-*.AppImage

    chmod 0755 squashfs-root
    # 
    # The following line does not work quite yet due to https://github.com/probonopd/go-appimage/issues/30
    # ./appimagetool-*-x86_64.AppImage deploy squashfs-root/usr/share/applications/dawcontrol.desktop
    ./appimagetool-*-x86_64.AppImage squashfs-root/ # Replace "1" with the actual version/build number

    mv DawControl-*-x86_64.AppImage DawControl-$VERSION-x86_64.AppImage

    # Test it
    #./DawControl-$VERSION-x86_64.AppImage 
popd

mv $BUILD_DIR/DawControl-$VERSION-x86_64.AppImage .
