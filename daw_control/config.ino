#include "daw_control.h"

#include <EEPROM.h>

#define CFG_EEPROM_ADDR   0

#define READ_CMD  'R'
#define KEY_CMD   'K'
#define MIDI_CMD  'M'
#define DEBNC_CMD 'D'
#define END_CMD   'E'
#define HUMAN_CMD 'H'

#define MAGIC_STR "DAW-CTL01"

typedef struct {
    char magic[10];
    cfg debounce;
    cfg buttons[BUTTONS_COUNT];
} cfg_storage;

extern button buttons[BUTTONS_COUNT];
extern unsigned int debounce_ms;

String num;
cfg tmp_cfg;
cfg_storage global_cfg;

void dump_cfg() {
    Serial.write("debounce: ");
    Serial.write(String(global_cfg.debounce.key).c_str());
    Serial.write("\n\r");
    for(int i = 0; i < BUTTONS_COUNT; ++i) {
        Serial.write("button ");
        Serial.write(String(i).c_str());
        Serial.write(": ");
        Serial.write(global_cfg.buttons[i].cmd);
        Serial.write(" - ");
        if(global_cfg.buttons[i].cmd == KEY_CMD) {
            Serial.write(global_cfg.buttons[i].key);
        } else {
            uint8_t c = global_cfg.buttons[i].key & 0xF;
            uint8_t n = (global_cfg.buttons[i].key >> 4) & 0x7F;
            uint8_t v = (global_cfg.buttons[i].key >> 12) & 0x7F;
            String s = "channel " + String(c) + ", note: " + String(n) + ", vel: " + String(v);
            Serial.write(s.c_str());
        }
        Serial.write("\n\r");
    }
}

void read_cfg() {
    Serial.write(global_cfg.debounce.cmd);
    Serial.write(String(global_cfg.debounce.key).c_str());
    Serial.write('-');
    for(int i = 0; i < BUTTONS_COUNT; ++i) {
        Serial.write(global_cfg.buttons[i].cmd);
        Serial.write(String(i).c_str());
        Serial.write(String((uint32_t)global_cfg.buttons[i].key).c_str());
        Serial.write('-');
    }
    Serial.write(END_CMD);
}

void cfg_bail() {
    Serial.println("unknown config command, aborting configuration");
}

void store_configs() {
    EEPROM.put(CFG_EEPROM_ADDR, global_cfg);
}

//false on success
bool apply_cfg(cfg *c, bool dostore) {
    bool modified = false;
    switch(c->cmd) {
        case KEY_CMD:
            if(c->key != global_cfg.buttons[c->button].key ||
                    c->cmd != global_cfg.buttons[c->button].cmd) {
                buttons[c->button].output = c->key;
                buttons[c->button].mode = BTN_KEY;
                global_cfg.buttons[c->button].cmd = KEY_CMD;
                global_cfg.buttons[c->button].key = c->key;
                modified = true;
            }
            break;
        case MIDI_CMD:
            if(c->key != global_cfg.buttons[c->button].key ||
                    c->cmd != global_cfg.buttons[c->button].cmd) {
                buttons[c->button].output = c->key;
                buttons[c->button].mode = BTN_MIDI;
                global_cfg.buttons[c->button].cmd = MIDI_CMD;
                global_cfg.buttons[c->button].key = c->key;
                modified = true;
            }
            break;
        case DEBNC_CMD:
            if(c->key != global_cfg.debounce.key && c->key > 0) {
                global_cfg.debounce.key = c->key;
                debounce_ms = c->key;
                modified = true;
            }
            break;
        default:
            return true;
    }
    if(dostore && modified) {
        store_configs();
    }
    INIT_CONFIG(c);
    return false;
}

void config_setup() {
    Serial.begin(9600);

    memcpy(global_cfg.magic, MAGIC_STR, sizeof(global_cfg.magic));

    INIT_BUTTON_CFG(global_cfg.debounce, DEBNC_CMD, -1, DEBOUNCE_MS);
    INIT_BUTTON_CFG(global_cfg.buttons[0], KEY_CMD, 0, 'a');
    INIT_BUTTON_CFG(global_cfg.buttons[1], KEY_CMD, 1, 'b');
    INIT_BUTTON_CFG(global_cfg.buttons[2], KEY_CMD, 2, 'c');
    INIT_BUTTON_CFG(global_cfg.buttons[3], KEY_CMD, 3, 'd');
    INIT_BUTTON_CFG(global_cfg.buttons[4], KEY_CMD, 4, 'e');

    cfg_storage stored;

    EEPROM.get(CFG_EEPROM_ADDR, stored);

    if(!strncmp(stored.magic, global_cfg.magic, sizeof(global_cfg.magic))) {
        apply_cfg(&(stored.debounce), false);
        for(int i = 0; i < BUTTONS_COUNT; ++i) {
            apply_cfg(&(stored.buttons[i]), false);
        }
    }
}

void begin_configure() {
    INIT_CONFIG((&tmp_cfg));
}

bool configure_device() {
    bool result = false;
    if(Serial.available() > 0) {
        int data = Serial.read();
        switch(tmp_cfg.state) {

            case CFG_BEGIN:
                switch(data) {
                    case READ_CMD:
                        read_cfg();
                        break;
                    case HUMAN_CMD:
                        dump_cfg();
                        break;
                    case END_CMD:
                        result = true;
                        break;
                    default:
                        tmp_cfg.cmd = data;
                        num = "";
                        tmp_cfg.state = CFG_CMD;
                }
                break;

            case CFG_CMD:
                if(isDigit(data)) {
                    num += (char)data;
                }
                switch(tmp_cfg.cmd) {
                    case KEY_CMD:
                    case MIDI_CMD:
                        tmp_cfg.button = num.toInt();
                        tmp_cfg.state = CFG_BUT;
                        num = "";
                        break;
                    case DEBNC_CMD:
                        if(!isDigit(data)) {
                            tmp_cfg.key = num.toInt();
                            if(apply_cfg(&tmp_cfg, true)) {
                                cfg_bail();
                                result = true;
                            }
                        }
                        break;
                    default:
                        cfg_bail();
                        result = true;
                        break;
                }
                break;

            case CFG_BUT:
                if(isDigit(data)) {
                    num += (char)data;
                }
                switch(tmp_cfg.cmd) {
                    case KEY_CMD:
                    case MIDI_CMD:
                        if(!isDigit(data)){
                            tmp_cfg.key = num.toInt();
                            if(apply_cfg(&tmp_cfg, true)) {
                                cfg_bail();
                                result = true;
                            }
                        }
                        break;
                    default:
                        cfg_bail();
                        result = true;
                        break;
                }

                break;

        }
    }
    return result;
}
