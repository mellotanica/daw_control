#ifndef __DAW_CONTROL_H__
#define __DAW_CONTROL_H__

/*******************
 * config constants
 */

#define DEBOUNCE_MS         50
#define CONFIG_TIMEOUT_MS   1000
#define BUTTONS_COUNT       5

/********************
 * global structures
 */

typedef enum {
    BTN_KEY,
    BTN_MIDI,
} button_mode;

typedef struct {
    int8_t pin;
    uint32_t output;
    button_mode mode;
    int8_t state;
    int8_t last_reading;
    unsigned long last_debounce_time;
} button;

#define INIT_BUTTON(__p__) { .pin = __p__, .output = 0, .mode = BTN_KEY, .state = HIGH, .last_reading = HIGH, .last_debounce_time = 0 }

typedef enum {
    CFG_BEGIN,
    CFG_CMD,
    CFG_BUT,
} cfg_states;

typedef struct {
    int cmd;
    uint32_t button;
    uint32_t key;
    cfg_states state;
} cfg;

#define INIT_BUTTON_CFG(__p__, __c__, __b__, __o__) ({  \
            __p__.cmd = __c__;                          \
            __p__.button = __b__;                       \
            __p__.key = __o__;                          \
            __p__.state = CFG_BEGIN;                    \
        })

#define INIT_CONFIG(__c__) ({                   \
            __c__->cmd = -1;                    \
            __c__->button = -1;                 \
            __c__->key = -1;                    \
            __c__->state = CFG_BEGIN;           \
        })

#endif //__DAW_CONTROL_H__
