#if ARDUINO > 10605
#include <Keyboard.h>
#endif

#include "daw_control.h"

typedef enum {
    MACHINE_RUNNING,
    MACHINE_CONFIG
} fms_state;

button buttons[BUTTONS_COUNT] = {
    INIT_BUTTON(2),
    INIT_BUTTON(3),
    INIT_BUTTON(4),
    INIT_BUTTON(5),
    INIT_BUTTON(6),
};

fms_state machine_state;

unsigned long config_start_time;

void setup() {
    for(unsigned int i = 0; i < BUTTONS_COUNT; ++i) {
        pinMode(buttons[i].pin, INPUT_PULLUP);
    }

    exec_setup();
    config_setup();

    machine_state = MACHINE_RUNNING;
}


void loop() {
    switch(machine_state){
        case MACHINE_RUNNING:
            for(unsigned int i = 0; i < BUTTONS_COUNT; ++i) {
                process_button(&buttons[i]);
            }
            if (Serial.available() > 0) {
                machine_state = MACHINE_CONFIG;
                begin_configure();
                config_start_time = millis();
            }
            break;
        case MACHINE_CONFIG:
            if(configure_device() || (millis() - config_start_time) > CONFIG_TIMEOUT_MS) {
                machine_state = MACHINE_RUNNING;
            }
            break;
    }
}
