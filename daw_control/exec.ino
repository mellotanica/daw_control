#if ARDUINO > 10605
#include <Keyboard.h>
#endif

#include "MIDIUSB.h"
#include "daw_control.h"

#define MIDI_CHNL 0
#define MIDI_VELO 127

unsigned int debounce_ms = DEBOUNCE_MS;

void exec_setup() {
    Keyboard.begin();
}

void exec_button(button *b, bool pressed) {
    switch(b->mode) {
        case BTN_KEY:
            if(pressed) {
                uint8_t c = b->output & 0xFF;
                uint8_t mod = (b->output >> 8) & 0xFF;
                for(int i = 0; i < 8; ++i) {
                    if(bitRead(mod, i) != 0) {
                        Keyboard.press(0x80 + i);
                    }
                }
                Keyboard.press(c);
                Keyboard.releaseAll();
            }
            break;
        case BTN_MIDI:
            uint8_t c = b->output & 0xF;
            midiEventPacket_t note;
            note.byte2 = (b->output >> 4) & 0x7F;
            note.byte3 = (b->output >> 12) & 0x7F;
            if(pressed) {
                note.header = 0x09;
                note.byte1 = 0x90 | c;
            } else {
                note.header = 0x08;
                note.byte1 = 0x80 | c;
            }
            MidiUSB.sendMIDI(note);
            MidiUSB.flush();
            break;
    }
}

void process_button(button *b) {
    int reading = digitalRead(b->pin);

    if (reading != b->last_reading) {
        b->last_debounce_time = millis();
    }

    if ((millis() - b->last_debounce_time) > debounce_ms) {
        if (reading != b->state) {
            b->state = reading;

            if (reading == LOW) {
                exec_button(b, true);
            } else {
                exec_button(b, false);
            }
        }
    }

    b->last_reading = reading;
}
