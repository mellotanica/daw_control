#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from .config import run_app

sys.exit(run_app())
