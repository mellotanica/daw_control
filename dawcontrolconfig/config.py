#!/usr/bin/env python3

import sys

from PySide2 import QtWidgets, QtCore, QtGui
import glob
import serial, serial.tools.list_ports
from .device import DeviceSelect
from .keyselect import KeySelect
from .midiselect import MidiSelect

class ButtonSetting(QtWidgets.QWidget):
    def __init__(self, num, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.number = num

        self.tabs = QtWidgets.QTabWidget()

        self.keyboard = KeySelect()
        self.tabs.addTab(self.keyboard, "key")

        self.midi = MidiSelect()
        self.tabs.addTab(self.midi, "midi")

        vlayout = QtWidgets.QVBoxLayout()
        vlayout.addWidget(QtWidgets.QLabel("Button {}".format(num+1)))
        vlayout.addWidget(self.tabs)
        self.setLayout(vlayout)

    def set_mode(self, isMidi, val):
        if isMidi:
            self.tabs.setCurrentWidget(self.midi)
            self.midi.set_value(val)
        else:
            self.tabs.setCurrentWidget(self.keyboard)
            self.keyboard.set_value(val)

    def get_config(self):
        if self.tabs.currentWidget() is self.midi:
            return self.midi.get_conf(self.number)
        else:
            return self.keyboard.get_conf(self.number)

class MainWindow(QtWidgets.QWidget):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)

        self.setFixedSize(900, 300)

        self.device = DeviceSelect()

        self.debounce = QtWidgets.QSpinBox()

        topLayout = QtWidgets.QHBoxLayout()
        topLayout.addWidget(QtWidgets.QLabel("Device: "))
        topLayout.addWidget(self.device)
        topLayout.addWidget(QtWidgets.QLabel("Debounce time(ms): "))
        topLayout.addWidget(self.debounce)

        centerLayout = QtWidgets.QHBoxLayout()
        self.buttons = []
        for i in range(5):
            self.buttons.append(ButtonSetting(i))
            centerLayout.addWidget(self.buttons[i])

        load = QtWidgets.QPushButton("Load", self)
        store = QtWidgets.QPushButton("Store", self)

        self.connect(load, QtCore.SIGNAL("clicked()"), self.load)
        self.connect(store, QtCore.SIGNAL("clicked()"), self.store)

        bottomLayout = QtWidgets.QHBoxLayout()
        bottomLayout.addWidget(load)
        bottomLayout.addWidget(store)

        vlayout = QtWidgets.QVBoxLayout()
        vlayout.addLayout(topLayout)
        vlayout.addLayout(centerLayout)
        vlayout.addLayout(bottomLayout)
        self.setLayout(vlayout)

        self.load()

    def load(self):
        s = self.device.getSerial()
        s.write(b'R')
        conf = s.read_until(b'E')
        s.close()
        conf = conf.decode('ascii')
        print("received conf: {}".format(conf))

        while len(conf) > 0 and conf[0] != 'E':
            cmd = conf[0]
            conf = conf[1:]
            vend = conf.find('-')
            if cmd == 'D':
                val = conf[:vend]
                self.debounce.setValue(int(val))
            else:
                btn = int(conf[0])
                val = int(conf[1:vend])
                if cmd == 'K':
                    self.buttons[btn].set_mode(False, val)
                elif cmd == 'M':
                    self.buttons[btn].set_mode(True, val)
            conf = conf[vend+1:]

    def store(self):
        try:
            conf = 'D{}-'.format(self.debounce.value())
            for b in self.buttons:
                conf += b.get_config() + "-"
            conf += 'E'
            print("sending conf: {}".format(conf))
            conf = conf.encode('ascii')

            s = self.device.getSerial()
            s.write(conf)
            s.close()

            self.load()
        except Exception as e:
            print("unable to store because: {}".format(e))

def run_app():
    app = QtWidgets.QApplication(sys.argv)
    widget = MainWindow()
    widget.show()
    return(app.exec_())

if __name__ == "__main__":
    sys.exit(run_app())
