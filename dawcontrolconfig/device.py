#!/usr/bin/env python3

from PySide2 import QtWidgets
import serial, serial.tools.list_ports

class DeviceSelect(QtWidgets.QComboBox):
    def __init__(self, parent=None):
        QtWidgets.QComboBox.__init__(self, parent)

        ports = serial.tools.list_ports.comports()
        d = -1
        for i in range(len(ports)):
            p = ports[i]
            if "Arduino" in p.description:
                self.addItem("{} ({})".format(p.description, p.device), p)
                d = i
                break
        if d >= 0:
            ports = ports[:d] + ports[d+1:]
        for p in ports:
            self.addItem("{} ({})".format(p.description, p.device), p)

    def getSerial(self):
        dev = self.itemData(self.currentIndex())
        s = serial.Serial(dev.device, timeout=1)
        s.read(s.in_waiting)
        return s
