from PySide2 import QtWidgets, QtCore

modifiers = {
    "CTRL":         0b00000001,
    "LEFT_CTRL":    0b00000001,
    "SHIFT":        0b00000010,
    "LEFT_SHIFT":   0b00000010,
    "ALT":          0b00000100,
    "LEFT_ALT":     0b00000100,
    "META":         0b00001000,
    "LEFT_META":    0b00001000,
    "LEFT_GUI":     0b00001000,
    "GUI":          0b00001000,
    "RIGHT_CTRL":   0b00010000,
    "RIGHT_SHIFT":  0b00100000,
    "RIGHT_ALT":    0b01000000,
    "RIGHT_META":   0b10000000,
    "RIGHT_GUI":    0b10000000,
}

keys = {
    "NUL": 0x00,
    "SOH": 0x01,
    "STX": 0x02,
    "ETX": 0x03,
    "EOT": 0x04,
    "ENQ": 0x05,
    "ACK": 0x06,
    "BEL": 0x07,
    "BS": 0x08,
    "HT": 0x09,
    "LF": 0x0A,
    "VT": 0x0B,
    "FF": 0x0C,
    "CR": 0x0D,
    "SO": 0x0E,
    "SI": 0x0F,
    "DLE": 0x10,
    "DC1": 0x11,
    "DC2": 0x12,
    "DC3": 0x13,
    "DC4": 0x14,
    "NAK": 0x15,
    "SYN": 0x16,
    "ETB": 0x17,
    "CAN": 0x18,
    "EM": 0x19,
    "SUB": 0x1A,
    "ESCAPE": 0x1B,
    "FS": 0x1C,
    "GS": 0x1D,
    "RS": 0x1E,
    "US": 0x1F,
    "SPACE": 0x20,
    "SPC": 0x20,
    "UP": 0xDA,
    "UP_ARROW": 0xDA,
    "DOWN": 0xD9,
    "DOWN_ARROW": 0xD9,
    "LEFT": 0xD8,
    "LEFT_ARROW": 0xD8,
    "RIGHT": 0xD7,
    "RIGHT_ARROW": 0xD7,
    "BACKSPACE": 0xB2,
    "TAB": 0xB3,
    "RETURN": 0xB0,
    "ENTER": 0xB0,
    "ESC": 0xB1,
    "INSERT": 0xD1,
    "INS": 0xD1,
    "DELETE": 0xD4,
    "DEL": 0xD4,
    "PAGE_UP": 0xD3,
    "PAGEUP": 0xD3,
    "PAGE_DOWN": 0xD6,
    "PAGEDOWN": 0xD6,
    "HOME": 0xD2,
    "END": 0xD5,
    "CAPS_LOCK": 0xC1,
    "CAPSLOCK": 0xC1,
    "F1": 0xC2,
    "F2": 0xC3,
    "F3": 0xC4,
    "F4": 0xC5,
    "F5": 0xC6,
    "F6": 0xC7,
    "F7": 0xC8,
    "F8": 0xC9,
    "F9": 0xCA,
    "F10": 0xCB,
    "F11": 0xCC,
    "F12": 0xCD,
    "F13": 0xF0,
    "F14": 0xF1,
    "F15": 0xF2,
    "F16": 0xF3,
    "F17": 0xF4,
    "F18": 0xF5,
    "F19": 0xF6,
    "F20": 0xF7,
    "F21": 0xF8,
    "F22": 0xF9,
    "F23": 0xFA,
    "F24": 0xFB,
}

def reverse_map(m):
    rm = {}
    for k in m.keys():
        v = m[k]
        if v not in rm:
            rm[v] = k
    return rm

reverse_modifiers = reverse_map(modifiers)
reverse_keys = reverse_map(keys)

class KeyLearn(QtWidgets.QDialog):
    def __init__(self, key, parent=None):
        QtWidgets.QDialog.__init__(self, parent)

        layout = QtWidgets.QVBoxLayout()

        self.key = key
        self.orig = key

        done = QtWidgets.QPushButton("Ok")
        cancel = QtWidgets.QPushButton("Cancel")
        text = ""
        if key is not None:
            text = KeySelect.val_to_str(self.key)
        self.label = QtWidgets.QLabel(text)

        layout.addWidget(self.label)

        hlayout = QtWidgets.QHBoxLayout()
        hlayout.addWidget(done)
        hlayout.addWidget(cancel)

        layout.addLayout(hlayout)

        self.setLayout(layout)

        self.connect(done, QtCore.SIGNAL("clicked()"), self.accept)
        self.connect(cancel, QtCore.SIGNAL("clicked()"), self.reject)

        self.label.setFocus()

    def keyPressEvent(self, event):
        key = event.key()
        if(key > 127):
            kname = QtCore.Qt.Key(key).name.decode('ascii').replace("Key_", "", 1).upper()
            if kname in keys:
                key = keys[kname]
            else:
                key = 0
        else:
            key = ord(event.text())
        mods = 0
        if event.modifiers() & QtCore.Qt.ShiftModifier:
            mods |= modifiers['SHIFT']
        if event.modifiers() & QtCore.Qt.ControlModifier:
            mods |= modifiers['CTRL']
        if event.modifiers() & QtCore.Qt.AltModifier:
            mods |= modifiers['ALT']
        if event.modifiers() & QtCore.Qt.MetaModifier:
            mods |= modifiers['META']

        self.key = (key & 0xFF) | (mods << 8)
        self.label.setText(KeySelect.val_to_str(self.key))

    def accept(self):
        self.done(self.key)

    def reject(self):
        self.done(self.orig)


class KeySelect(QtWidgets.QWidget):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)

        kblayout = QtWidgets.QVBoxLayout()
        self.text = QtWidgets.QLineEdit()
        learn = QtWidgets.QPushButton("Learn")
        kblayout.addWidget(self.text)
        kblayout.addWidget(learn)
        self.setLayout(kblayout)

        self.connect(learn, QtCore.SIGNAL("clicked()"), self.learn)
    
    @staticmethod
    def val_to_str(val):
        vstr = ""
        char = val & 0xFF
        mods = (val >> 8) & 0xFF
        for m in reverse_modifiers.keys():
            if (mods & m) != 0:
                vstr += "{} ".format(reverse_modifiers[m])
        if char in reverse_keys:
            vstr += reverse_keys[char]
        else:
            vstr += chr(char)
        return vstr

    def str_to_val(self, vstr):
        char = 0
        mods = 0
        if len(vstr) == 1:
            char = ord(vstr)
        else:
            tokens = vstr.split(' ')
            for t in tokens[:-1]:
                if len(t) > 0:
                    tok = t.upper()
                    if tok in modifiers:
                        mods |= modifiers[tok]
                    else:
                        QtWidgets.QMessageBox.warning(self, "Input Error", 
                                "Unkown modifier '{}'".format(t), 
                                QtWidgets.QMessageBox.StandardButton.Ok)
                        raise TypeError("unknown modifier '{}'".format(t))
            if tokens[-1] in keys:
                char = keys[tokens[-1]]
            else:
                if len(tokens[-1]) == 0:
                    QtWidgets.QMessageBox.warning(self, "Input Error", 
                            "Missing the main key (if you meant to use a space with some modifiers please use the SPACE identifier)", 
                            QtWidgets.QMessageBox.StandardButton.Ok)
                    raise TypeError("missing the main key")
                elif len(tokens[-1]) > 1:
                    QtWidgets.QMessageBox.warning(self, "Input Error", 
                            "Unkown special key '{}'".format(tokens[-1]), 
                            QtWidgets.QMessageBox.StandardButton.Ok)
                    raise TypeError("unknown special key '{}'".format(tokens[-1]))
                char = ord(tokens[-1])
        val = char | (mods << 8)
        return val

    def get_conf(self, num):
        return "K{}{}".format(num, self.str_to_val(self.text.text()))

    def set_value(self, val):
        self.text.setText(KeySelect.val_to_str(val))

    def learn(self):
        try:
            v = self.str_to_val(self.text.text())
        except:
            v = None
        lrn = KeyLearn(v)
        self.set_value(lrn.exec())

