from PySide2 import QtWidgets

class MidiSelect(QtWidgets.QWidget):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)

        midlayout = QtWidgets.QVBoxLayout()
        self.channel = QtWidgets.QSpinBox()
        self.channel.setRange(1,16)
        chl = QtWidgets.QHBoxLayout()
        chl.addWidget(QtWidgets.QLabel("Channel: "))
        chl.addWidget(self.channel)
        midlayout.addLayout(chl)
        self.note = QtWidgets.QSpinBox()
        self.note.setRange(0, 127)
        ntl = QtWidgets.QHBoxLayout()
        ntl.addWidget(QtWidgets.QLabel("Note: "))
        ntl.addWidget(self.note)
        midlayout.addLayout(ntl)
        self.velo = QtWidgets.QSpinBox()
        self.velo.setRange(0, 127)
        self.velo.setValue(127)
        vll = QtWidgets.QHBoxLayout()
        vll.addWidget(QtWidgets.QLabel("Velocity: "))
        vll.addWidget(self.velo)
        midlayout.addLayout(vll)

        self.setLayout(midlayout)

    def set_value(self, val):
        chan = (val & 0xF) + 1
        note = (val >> 4) & 0x7F
        velo = (val >> 12) & 0x7F
        if chan > 0 and chan <= 16:
            self.channel.setValue(chan)
        if note >= 0 and note < 128:
            self.note.setValue(note)
        if velo >= 0 and velo < 128:
            self.velo.setValue(velo)

    def get_conf(self, num):
        val = (self.velo.value() & 0x7F) << 12
        val |= (self.note.value() & 0x7F) << 4
        val |= (self.channel.value() - 1) & 0xF
        return "M{}{}".format(num, val)
