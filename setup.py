#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
setup(
    name = 'dawcontrol',
    version = '',
    description = 'daw control configurator utility',
    packages = find_packages(include=['dawcontrolconfig', 'dawcontrolconfig.*']),
    entry_points = {
        "console_scripts": ['dawcontrol=dawcontrolconfig.config:run_app'],
    },
    install_requires = [
        "PySide2>=5.15, <5.16",
        "pyserial>=3.5, <3.6",
        "cython",
    ],
)

